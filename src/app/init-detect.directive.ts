import {AfterViewInit, Directive, ElementRef, EventEmitter, Output} from '@angular/core';

@Directive({
  selector: '[appInitDetect]'
})
export class InitDetectDirective implements AfterViewInit {
  @Output() initDetected = new EventEmitter<ElementRef>();

  constructor(
    private rootRef: ElementRef
  ) {
  }

  ngAfterViewInit(): void {
    this.initDetected.emit(this.rootRef);
  }
}
