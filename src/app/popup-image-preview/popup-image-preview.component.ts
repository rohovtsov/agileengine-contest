import {Component, HostBinding, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {PopupEvent, PopupListener, PopupsService} from '../popups.service';
import {ApiImageFull} from '../api/interface/images';
import {ApiService} from '../api/api.service';
import {ScrollbarLockService} from '../scrollbar-lock.service';

@Component({
  selector: 'app-popup-image-preview',
  templateUrl: './popup-image-preview.component.html',
  styleUrls: ['./popup-image-preview.component.scss']
})
export class PopupImagePreviewComponent implements OnInit, OnDestroy, PopupListener {
  @Input() popupId: string;
  @HostBinding('class.is-popup-opened') isPopupOpen: boolean;
  @HostBinding('class.is-image-loaded') isLoaded = false;
  @HostBinding('style.--max-width') calculatedMaxWidth: number;
  image: PopupImage;

  constructor(
    private popupsService: PopupsService,
    private scrollbarLockService: ScrollbarLockService,
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
    this.popupsService.registerListener(this.popupId, this);
  }

  ngOnDestroy(): void {
    this.popupsService.removeListener(this);
  }

  private setOpenPopup(isOpen, bundle?: any) {
    if (isOpen === this.isPopupOpen) {
      return;
    }

    this.isPopupOpen = isOpen;

    if (isOpen && bundle) {
      this.onPopupOpened(bundle as ApiImageFull);
    }

    this.scrollbarLockService.lockByKey('popup-' + this.popupId, isOpen);
    this.triggerOpenChangeEvent(isOpen, bundle);
  }

  private onPopupOpened(image: ApiImageFull) {
    this.image = null;
    this.isLoaded = false;

    this.apiService.getImage(image.id).subscribe((imageFull) => {
      const htmlImage = new Image();
      htmlImage.src = imageFull.fullPicture;
      htmlImage.addEventListener('load', () => {
        this.image = {
          ...imageFull,
          width: htmlImage.naturalWidth,
          height: htmlImage.naturalHeight,
        };
        this.calculatedMaxWidth = this.calculateImageWidth(htmlImage.naturalWidth, htmlImage.naturalHeight);
        this.isLoaded = true;
      });
    });
  }

  private calculateImageWidth(imageWidth: number, imageHeight: number): number {
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;

    const windowRatio = windowWidth / windowHeight;
    const imageRatio = imageWidth / imageHeight;

    if (windowRatio >= imageRatio) {
      return windowHeight * imageRatio;
    } else {
      return windowWidth;
    }
  }

  @HostListener('document:visibilitychange')
  @HostListener('document:focus')
  @HostListener('window:resize')
  onResize() {
    if (this.image) {
      this.calculatedMaxWidth = this.calculateImageWidth(this.image.width, this.image.height);
    }
  }


  private triggerOpenChangeEvent(isOpen, bundle?: any) {
    const event = new PopupEvent(this.popupId, isOpen, bundle);
    this.popupsService.triggerOpenChangeEvent(event);
  }

  onBackgroundClick() {
    this.setOpenPopup(false);
  }

  onCloseClick() {
    this.setOpenPopup(false);
  }

  onExternalClose() {
    this.setOpenPopup(false);
  }

  onExternalOpen(bundle?: any) {
    this.setOpenPopup(true, bundle);
  }
}

interface PopupImage extends ApiImageFull {
  width: number;
  height: number;
}
