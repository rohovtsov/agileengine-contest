import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScrollbarLockService {
  private className = 'scrollbar-is-locked';
  private locksByKeys: any = {};
  public isLocked = false;
  private scrollBarWidth: number;
  private scrollBarWidthAtSize: number;

  constructor() {
    this.updateScrollBarWidth();
  }

  updateScrollBarWidth() {
    if (this.scrollBarWidthAtSize !== window.innerWidth) {
      this.scrollBarWidthAtSize = window.innerWidth;
      this.scrollBarWidth = getScrollBarWidth();
    }

    document.documentElement.style.setProperty('--scrollbar-width', this.scrollBarWidth + 'px');
  }

  calcScrollBarWidth() {
    const body = document.body;
    const isBodyScrollable = body.scrollHeight > window.innerHeight;
    const actualScrollBarWidth = isBodyScrollable ? window.innerWidth - document.documentElement.clientWidth : 0;

    if (actualScrollBarWidth === 0) {
      this.updateScrollBarWidth();
      return this.scrollBarWidth;
    } else {
      return Math.max(actualScrollBarWidth, this.scrollBarWidth);
    }
  }

  destroyAllLockers() {
    this.locksByKeys = {};
    this.updateLocked();
  }

  createLocker(key: string): ScrollbarLocker {
    return new ScrollbarLocker(this, key);
  }

  destroyLocker(locker: ScrollbarLocker) {
    delete this.locksByKeys[locker.key];
    this.updateLocked();
  }

  lockByKey(key: string, isLocked) {
    this.locksByKeys[key] = isLocked;
    this.updateLocked();
  }

  private updateLocked() {
    const isLocked = !!Object.keys(this.locksByKeys).find((key) => {
      return !!this.locksByKeys[key];
    });

    this.toggleLock(isLocked);
  }

  private toggleLock(isLock) {
    if (this.isLocked === isLock) {
      return;
    }

    this.isLocked = isLock;

    const body = document.body;

    if (isLock) {
      const scrollbarWidth = this.calcScrollBarWidth();
      body.style.marginRight = scrollbarWidth + 'px';
    } else {
      body.style.marginRight = null;
    }

    body.classList.toggle(this.className, isLock);
  }
}

export class ScrollbarLocker {
  constructor(
    private service: ScrollbarLockService,
    public key: string
  ) { }

  toggleLock(isLocked: boolean) {
    this.service.lockByKey(this.key, isLocked);
  }

  destroy() {
    this.service.destroyLocker(this);
  }
}

const getScrollBarWidth = () => {
  const inner = document.createElement('p');
  inner.style.width = '100%';
  inner.style.height = '200px';

  const outer = document.createElement('div');
  outer.style.position = 'absolute';
  outer.style.top = '0px';
  outer.style.left = '0px';
  outer.style.visibility = 'hidden';
  outer.style.pointerEvents = 'none';
  outer.style.width = '200px';
  outer.style.height = '150px';
  outer.style.overflow = 'hidden';
  outer.appendChild(inner);

  document.body.appendChild(outer);
  const w1 = inner.offsetWidth;
  outer.style.overflow = 'scroll';
  let w2 = inner.offsetWidth;

  if (w1 === w2) {
    w2 = outer.clientWidth;
  }

  document.body.removeChild(outer);

  return (w1 - w2);
};
