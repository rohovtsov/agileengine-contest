import {ChangeDetectorRef, Component} from '@angular/core';
import {PageLoaderService} from './page-loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isLoaded: boolean;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private pageLoader: PageLoaderService
  ) {
    this.pageLoader.isLoaded$.subscribe((isLoaded) => {
      this.isLoaded = isLoaded;
      this.changeDetectorRef.detectChanges();
    });
  }
}
