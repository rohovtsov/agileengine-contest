import {ChangeDetectorRef, Component, HostListener, OnInit} from '@angular/core';
import {ApiService} from '../api/api.service';
import {ApiImage} from '../api/interface/images';
import {PageLoaderService} from '../page-loader.service';
import {PopupsService} from '../popups.service';
import {ImagesService} from '../images.service';
import {ImagesPagination} from './images-pagination';

@Component({
  selector: 'app-page-images',
  templateUrl: './page-images.component.html',
  styleUrls: ['./page-images.component.scss']
})
export class PageImagesComponent implements OnInit {
  images: ApiImage[];
  pagination: ImagesPagination;

  constructor(
    private apiService: ApiService,
    public pageLoader: PageLoaderService,
    private popupsService: PopupsService,
    private imagesService: ImagesService,
    private changeDetectorRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.pagination = new ImagesPagination(
      this.imagesService,
      1,
      (images) => {
        this.images = images;
        this.changeDetectorRef.detectChanges();

        setTimeout(() => {
          this.triggerMorePages();
        }, 0);
      }
    );
  }

  onImageExpand(image: ApiImage) {
    this.popupsService.openPopup('image-preview', image);
  }

  @HostListener('document:visibilitychange')
  @HostListener('document:focus')
  @HostListener('window:resize')
  @HostListener('window:scroll')
  @HostListener('window:pageanimated')
  onPageScroll() {
    this.triggerMorePages();
  }

  private triggerMorePages() {
    const scrollTop = window.scrollY;
    const scrollHeight = document.body.scrollHeight;
    const pageEndScrollTop = (scrollHeight - window.innerHeight);
    const isAddPages = scrollTop + window.innerHeight > pageEndScrollTop;

    if (isAddPages) {
      this.pagination.addPage();
    }
  }
}
