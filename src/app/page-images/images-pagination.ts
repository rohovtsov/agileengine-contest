import {ImagesService} from '../images.service';
import {ApiImage} from '../api/interface/images';

export class ImagesPagination {
  private isLoading = false;

  constructor(
    private imagesService: ImagesService,
    private pagesCount: number,
    private onImagesUpdated: (images: ApiImage[]) => void
  ) {
    this.loadPages(this.pagesCount);
  }

  public addPage() {
    this.loadPages(this.pagesCount + 1);
  }

  private loadPages(pagesCount) {
    if (this.isLoading || pagesCount > this.imagesService.pagesCount) {
      return;
    }

    this.isLoading = true;

    this.imagesService.getImagesList(pagesCount).subscribe((images) => {
      this.isLoading = false;
      this.pagesCount = pagesCount;
      this.onImagesUpdated(images);
    });
  }
}
