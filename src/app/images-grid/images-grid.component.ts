import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiImage} from '../api/interface/images';

@Component({
  selector: 'app-images-grid',
  templateUrl: './images-grid.component.html',
  styleUrls: ['./images-grid.component.scss']
})
export class ImagesGridComponent implements OnInit {
  @Output() clickImage = new EventEmitter<ApiImage>();
  @Input() images: ApiImage[];

  constructor() { }

  ngOnInit(): void {
  }

  onImageClick(image: ApiImage) {
    this.clickImage.emit(image);
  }

  trackByImageItem(index: number, image: ApiImage): string {
    return image.id;
  }
}
