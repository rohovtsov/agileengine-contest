import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {map, tap} from 'rxjs/operators';

export class ApiTokenRequester {
  private cachedToken: string;

  constructor(
    private httpClient: HttpClient,
    private url: string,
    private apiKey: string
  ) {
  }

  private requestToken(): Observable<string> {
    return this.httpClient.post(this.url, {
      apiKey: this.apiKey
    }).pipe(
      map((response: any) => {
        return response && typeof response.token === 'string' ? response.token : null;
      })
    );
  }

  public hasCachedToken(): boolean {
    return !!this.cachedToken;
  }

  public getCachedToken(): string {
    return this.cachedToken;
  }

  public getRequestedToken(): Observable<string> {
    return this.requestToken().pipe(
      tap((token) => this.cachedToken = token)
    );
  }

  public getToken(): Observable<string> {
    if (this.hasCachedToken()) {
      return of(this.getCachedToken());
    } else {
      return this.getRequestedToken();
    }
  }
}
