export interface PagePosition {
  index: number;
  length: number;
  pageIndex: number;
}
