import {PagePosition} from './page-poistion';

export interface ImagesPage {
  hasMore: boolean;
  page: number;
  pageCount: number;
  images: ApiImage[];
}

export interface ApiImage {
  croppedPicture: string;
  id: string;
  position?: PagePosition;
}

export interface ApiImageFull extends ApiImage {
  id: string;
  author: string;
  camera: string;
  croppedPicture: string;
  fullPicture: string;
  tags: string[];
}

export const parseImagesPage = (object: any): ImagesPage => {
  const pageIndex = object.page;

  return {
    hasMore: object.hasMore,
    page: pageIndex,
    pageCount: object.pageCount,
    images: object.pictures ? object.pictures.map((picture, index) => {
      return parseApiImage(picture, {
        index,
        length: object.pictures.length,
        pageIndex,
      });
    }) : []
  };
};

export const parseApiImage = (object: any, position?: PagePosition): ApiImage => {
  return {
    id: object.id,
    croppedPicture: object.cropped_picture,
    position: position ?? null
  };
};

export const parseApiImageFull = (object: any): ApiImageFull => {
  return {
    ...parseApiImage(object),
    author: object.author,
    camera: object.camera,
    fullPicture: object.full_picture,
    tags: typeof object.tags === 'string' ? object.tags
      .split(' ')
      .filter((item) => item.length > 0)
      .map((tag) => tag.trim()) : []
  };
};
