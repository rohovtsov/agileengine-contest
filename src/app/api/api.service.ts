import { Injectable } from '@angular/core';
import {ApiTokenRequester} from './api-token-requester';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {API_AUTH_URL, API_IMAGE_URL_TEMPLATE, API_IMAGES_URL, API_KEY} from './api-constants';
import {Observable, of, throwError, zip} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {formatString} from '../helpers/formatter';
import {ApiImageFull, ImagesPage, parseApiImageFull, parseImagesPage} from './interface/images';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private tokenRequester: ApiTokenRequester;

  constructor(
    private httpClient: HttpClient
  ) {
    this.tokenRequester = new ApiTokenRequester(this.httpClient, API_AUTH_URL, API_KEY);
  }

  public getImagesPage(page: number = 1): Observable<ImagesPage> {
    return this.authorizedRequest({
      url: API_IMAGES_URL,
      params: {
        page: page.toString()
      }
    }).pipe(
      map((response) => {
        return response ? parseImagesPage(response) : null;
      })
    );
  }

  public getImage(id: string): Observable<ApiImageFull> {
    return this.authorizedRequest({
      url: formatString(API_IMAGE_URL_TEMPLATE, { id })
    }).pipe(
      map((response) => {
        return response ? parseApiImageFull(response) : null;
      })
    );
  }

  private handleAuthorizationError(err: HttpErrorResponse, params: AuthorizedRequestParams): Observable<any> {
    const tokenAlreadyRequested = params.forceTokenRequest;

    if ((err.status === 401 || err.status === 403) && !tokenAlreadyRequested) {
      return this.authorizedRequest({
        ...params,
        forceTokenRequest: true
      });
    }

    return throwError(err);
  }

  private authorizedRequest<T = any>(params: AuthorizedRequestParams): Observable<T> {
    const token$ = params.forceTokenRequest ? this.tokenRequester.getRequestedToken() : this.tokenRequester.getToken();

    return token$.pipe((
      mergeMap((token) => {
        const headers = new HttpHeaders({
          Authorization: 'Bearer ' + token
        });

        return this.httpClient.get(params.url, {
          headers,
          params: params.params
        }).pipe(
          catchError((err) => this.handleAuthorizationError(err, params))
        );
      })
    )) as Observable<T>;
  }
}

interface AuthorizedRequestParams {
  url: string;
  params?: any;
  forceTokenRequest?: boolean;
}
