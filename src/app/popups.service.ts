import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupsService {
  private listeners: ListenerBySlug[] = [];
  private openedPopupsMap = {};
  private openedPopupsHistory = [];

  constructor() {
    window.addEventListener('keyup', this.onKeyUp.bind(this));
  }

  registerListener(slug: string, listener: PopupListener) {
    this.listeners.push(new ListenerBySlug(
      slug,
      listener
    ));
  }

  removeListener(listener: PopupListener) {
    this.listeners = this.listeners.filter((item) => item.listener !== listener);
  }

  private getListener(slug: string): ListenerBySlug {
    return this.listeners.find((item) => {
      return item.slug === slug;
    });
  }

  openPopup(slug: string, bundle?: any) {
    this.setPopupOpen(slug, true, bundle);
  }

  closePopup(slug: string) {
    this.setPopupOpen(slug, false);
  }

  private setPopupOpen(slug: string, isOpen: boolean, bundle?: any) {
    const foundItem = this.getListener(slug);

    if (!foundItem) {
      return;
    }

    if (isOpen) {
      foundItem.listener.onExternalOpen(bundle);
    } else {
      foundItem.listener.onExternalClose();
    }
  }

  private onKeyUp(event) {
    if (event && event.key && event.key.toLowerCase() === 'escape') {
      this.closeLastOpenedPopup();
    }
  }

  triggerOpenChangeEvent(event: PopupEvent) {
    this.openedPopupsMap[event.popupId] = event.isOpen;
    this.saveOpenChangeEventToHistory(event);
  }

  private saveOpenChangeEventToHistory(event: PopupEvent) {
    const existingPopupId = this.openedPopupsHistory.indexOf(event.popupId);

    if (existingPopupId >= 0) {
      this.openedPopupsHistory.splice(existingPopupId, 1);
    }

    if (event.isOpen) {
      this.openedPopupsHistory.push(event.popupId);
    }
  }

  private closeLastOpenedPopup() {
    if (!this.openedPopupsHistory.length) {
      return;
    }

    const lastPopupKey = this.openedPopupsHistory[this.openedPopupsHistory.length - 1];
    this.closePopup(lastPopupKey);
  }

  isPopupOpen(popupId: string = null) {
    if (popupId) {
      return !!this.openedPopupsMap[popupId];
    } else {
      return !!Object.keys(this.openedPopupsMap).find(key => !!this.openedPopupsMap[key]);
    }
  }

  closePopups() {
    this.listeners.forEach((item) => {
      item.listener.onExternalClose();
    });
  }
}

class ListenerBySlug {
  constructor(
    public slug: string,
    public listener: PopupListener
  ) { }
}

export interface PopupListener {
  onExternalOpen(bundle?: any);
  onExternalClose();
}

export class PopupEvent {
  constructor(
    public popupId: string,
    public isOpen,
    public bundle?: any
  ) {
  }
}
