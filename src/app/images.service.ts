import { Injectable } from '@angular/core';
import {ApiService} from './api/api.service';
import {ApiImage, ImagesPage} from './api/interface/images';
import {Observable, of, zip} from 'rxjs';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImagesService {
  pages: ImagesPage[] = [];
  pagesCount = Infinity;

  constructor(
    private apiService: ApiService
  ) { }

  public getImagesPage(page: number): Observable<ImagesPage> {
    const pageIndex = page - 1;

    if (pageIndex >= 0 && pageIndex < this.pages.length) {
      return of(this.pages[pageIndex]);
    } else {
      return this.apiService.getImagesPage(page).pipe(
        tap(pageObject => {
          this.pagesCount = pageObject.pageCount;
          this.pages[pageIndex] = pageObject;
        })
      );
    }
  }

  public getImagesList(toPage: number): Observable<ApiImage[]> {
    const images$ = [];

    for (let i = 1; i < Math.min(toPage, this.pagesCount) + 1; i++) {
      images$.push(this.getImagesPage(i));
    }

    return zip(...images$).pipe(
      map(imagePages => {
        const images: ApiImage[] = [];

        imagePages.forEach((page: ImagesPage) => {
          images.push(...page.images);
        });

        return images;
      })
    );
  }

}
