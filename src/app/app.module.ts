import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ImagesGridComponent } from './images-grid/images-grid.component';
import { PageImagesComponent } from './page-images/page-images.component';
import { InitDetectDirective } from './init-detect.directive';
import { PopupImagePreviewComponent } from './popup-image-preview/popup-image-preview.component';

@NgModule({
  declarations: [
    AppComponent,
    ImagesGridComponent,
    PageImagesComponent,
    InitDetectDirective,
    PopupImagePreviewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
