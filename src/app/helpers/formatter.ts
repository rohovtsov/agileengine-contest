export const formatString = (value: string, values: { [s: string]: any }): string => {
  if (typeof value === 'number' && !isNaN(value)) {
    return `${value}`;
  }
  if (typeof value !== 'string') {
    return '';
  }
  if (!values || typeof values !== 'object') {
    values = {};
  }
  return value.replace(
    /{{\w+}}/g,
    (placeholder) => values[placeholder.substring(2, placeholder.length - 2)] || placeholder,
  );
};
