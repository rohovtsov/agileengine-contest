import {Injectable} from '@angular/core';
import {Subject, Subscription} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageLoaderService {
  private isLoadedSubject = new Subject<boolean>();
  public isLoaded$ = this.isLoadedSubject.asObservable();
  private postponedAnimationEnd: any;

  constructor() {
  }

  markAsUnloaded() {
    this.setLoaded(false);
    this.setAnimationEnded(false);
  }

  markAsLoaded() {
    window.scrollTo(0, 0);

    setTimeout(() => {
      this.setLoaded(true);
      this.postponeAnimationEnd();
    }, 0);
  }

  private postponeAnimationEnd() {
    clearTimeout(this.postponedAnimationEnd);

    this.postponedAnimationEnd = setTimeout(() => {
      this.setAnimationEnded(true);
    }, 510);
  }

  private setAnimationEnded(isEnded) {
    if (isEnded) {
      window.dispatchEvent(new Event('pageanimated'));
    }
  }

  private setLoaded(isLoaded) {
    this.isLoadedSubject.next(isLoaded);
  }
}
