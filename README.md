# AgileEngine Contest

This project is a contest to AgileEngine. It was finally completed in 5 and a half hours.
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

## Development server

Run `npm install && ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Note
This project contains automatic pagination dependent on scrolling position
